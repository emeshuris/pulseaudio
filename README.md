Readme
======

Launching pulseaudio
```
echo "autospawn = no" > ~/.config/pulse/client.conf
pulseaudio --kill
rm -rf ~/.config/pulse/
docker-compose up -d
```

If you wish your host system to use your docker pulseaudio server, e.g.
accepting Fn volume Up/Down and On/Off keys, issue the following command
```
xprop -root -f PULSE_SERVER 8s -set PULSE_SERVER "tcp:localhost:4713"
```

To remove pulseaudio properties from environment variables
```
pax11publish -e
```

To test whether pulseaudio is working
```
PULSE_SERVER="tcp:localhost:4713" pactl list
PULSE_SERVER="tcp:localhost:4713" alsamixer
PULSE_SERVER="tcp:localhost:4713" mplayer some.avi
```

To see what is being used by pulseaudio
```
$ netstat -lp |grep -i pulseaudio
unix  2      [ ACC ]     STREAM     LISTENING     29632    2147/pulseaudio      /tmp/.esd-1000/socket
unix  2      [ ACC ]     STREAM     LISTENING     29635    2147/pulseaudio      /run/user/1000/pulse/native
```

If you wish to disable annoying "popping" notification sounds
```
gsettings set org.gnome.desktop.sound event-sounds false
```
